import random

from discord_base_bots.base import run_bot
from discord_base_bots.fighter import Fighter


class DickPuncher(Fighter):
    bot_name = "dick-puncher"
    hot_word = "Dick Puncher"

    #############################
    #
    # Your round actions
    #
    #############################
    def action(self, *args):
        """What do you want to do this round?"""
        return random.choice(["attack", "defend"])

    def read_reaction(self, reaction):
        """Read how your opponent reacted to *YOUR* round action."""
        if f"{self.opponent} blocks" in reaction:
            return f"{self.hot_word} staggers back"

    #############################
    #
    # Opponent round actions
    #
    #############################
    def process_opponent_action(self, action):
        """View the action your opponent took.

        Args:
            action (str): The action your opponent took this round.

        Returns:
            Optional[str]: A text response to the action.
        """
        if action.endswith("defend"):
            return "_searches your defence_"
        elif action.endswith("attack"):
            return "_attempts to counter_"

    #############################
    #
    # End conditions
    #
    #############################
    def on_victory(self, data, message):
        return ["_throws down weapon_",
                "Consider your dick... Punched!"]

    def on_loss(self, data, message):
        return "It seems I've punched my own dick."

    def on_timeout(self, data, message):
        return "Where is my fluffer?"


if __name__ == "__main__":
    from pathlib import Path
    path = Path(".env").resolve()
    run_bot(DickPuncher)
