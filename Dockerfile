FROM python:3.8

WORKDIR /opt
COPY . /opt/

RUN pip install -r requirements.txt

CMD ["python", "bot.py"]
